"use strict";

const path = require("path");

const config = require("config");
const glob = require("glob");

const taskAA = require("./tasks/taskAA");

const exceptionrulesDir = config.get("exceptionrules.dir");
const outputDir = config.get("output.dir");

const inputGlob = path.join(exceptionrulesDir, "*.txt");

// These recipes are temporarily hard-coded as this code caters a special case
// Todo: Next step, add mechanism to read recipe files
const task = new taskAA([{
  inputFiles: glob.sync(inputGlob, {
    ignore: [
      "exceptionrules.txt",
      "exceptionrules-no-third-party-tracking.txt"
    ].map(file => path.join(exceptionrulesDir, file))
  }),
  outputFile: path.join(outputDir, "samsung_internet_browser-adblock.txt"),
  partnersToInclude: new Set([
    "1771032db7d723d7",
    "743865c88b4a50d7",
    "c7cdd2fa8a3d0b2a",
    "627d41fb6de11f94",
    "89957ff1e879eb7f"
  ])
}, {
  inputFiles: glob.sync(inputGlob, {
    ignore: [
      "exceptionrules.txt",
      "exceptionrules-no-third-party-tracking.txt"
    ].map(file => path.join(exceptionrulesDir, file))
  }),
  outputFile: path.join(outputDir, "samsung_internet_browser-adblock_plus.txt"),
  partnersToInclude: new Set([
    "1771032db7d723d7",
    "743865c88b4a50d7",
    "c7cdd2fa8a3d0b2a",
    "627d41fb6de11f94",
    "89957ff1e879eb7f",
    "4b7133f644c78afd",
    "ec725ef475df5236",
    "2b3e85777627e764",
    "9df9943c03442951",
    "501a98900addb890",
    "25a5b2915c0c608d",
    "bcb8c89e6cddf972",
    "dbbec511a00713f9"
  ])
}, {
  inputFiles: glob.sync(inputGlob, {
    ignore: [
      "exceptionrules.txt",
      "exceptionrules-no-third-party-tracking.txt"
    ].map(file => path.join(exceptionrulesDir, file))
  }),
  outputFile: path.join(outputDir, "samsung_internet_browser-crystal.txt"),
  partnersToInclude: new Set([
    "1771032db7d723d7",
    "743865c88b4a50d7",
    "c7cdd2fa8a3d0b2a",
    "627d41fb6de11f94",
    "89957ff1e879eb7f",
    "4b7133f644c78afd",
    "ec725ef475df5236",
    "2b3e85777627e764",
    "9df9943c03442951",
    "501a98900addb890",
    "25a5b2915c0c608d",
    "bcb8c89e6cddf972",
    "dbbec511a00713f9"
  ])
}, {
  inputFiles: glob.sync(inputGlob, {
    ignore: [
      "exceptionrules.txt",
      "exceptionrules-no-third-party-tracking.txt",
      "exceptionrules-mobile.txt"
    ].map(file => path.join(exceptionrulesDir, file))
  }),
  outputFile: path.join(outputDir, "aa_google.txt"),
  partnersToInclude: new Set([
    "1771032db7d723d7"
  ])
}]);

(async () => {
  await task.generate();
})();
