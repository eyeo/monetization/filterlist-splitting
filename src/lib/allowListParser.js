"use strict";

const PARTNER_TOKEN_PATTERN = /^!:partner_token=([a-zA-Z\d-.\s]+)$/;
const PARTNER_ID_PATTERN = /^!:partner_id=([a-z\d]{16})$/;
const TYPE_PATTERN = /^!:type=(partner|qa|combined|temporary)$/;
const FORUM_LINK_PATTERN = /^!:forum=(https:\/\/(?:forum\.adblockplus\.org\/|adblockplus\.org\/forum\/)viewtopic\.php\?(?:f=12&)?[tp]=\d+(?:#p\d+)?)$/;

class AllowlistParser {

  static createBlock(lineNumber) {
    return {
      rules: [],
      comments: [],
      lines: [],
      lineNumber
    };
  }

  static detectedNewBlock(line) {
    return line.startsWith("!:partner_token=")
        || line.startsWith("!:type=combined")
        || line.startsWith("!:type=qa")
        || line.startsWith("!:type=temporary");
  }

  static parseBlocks(lines) {
    const blocks = [];
    
    let block;
    
    for (let lineNumber = 0; lineNumber < lines.length; lineNumber += 1) {
      const lineNormalized = lines[lineNumber].trim();

      // Push block when new block was detected or
      // end of file is reached
      if (AllowlistParser.detectedNewBlock(lineNormalized)
          || lineNumber === lines.length-1) {
        if (block) {
          blocks.push(block);
        }
        // Create new empty block.
        block = AllowlistParser.createBlock(lineNumber+1);
        block.comments.push(lineNormalized);
        block.lines.push(lineNormalized);
        continue;
      }

      // Handle comments and rules, empty lines are ignored
      if (lineNormalized.substr(0, 1) === "!") {
        block.comments.push(lineNormalized);
        block.lines.push(lineNormalized);
      } else if (lineNormalized !== "") {
        block.rules.push(lineNormalized);
        block.lines.push(lineNormalized);
      }
    }

    return blocks;
  }

  /**
   * Add single violation to a given block.
   *
   * @param {Object} block Block that caused the violation.
   * @param {string} type Type of the violation.
   * @param {string} message Description of the violation.
   *
   * @todo Make static and/or scope per block.
   */
  static addViolation(block, type, message) {
    block.violations = block.violations || [];
    block.violations.push({type, message});
  }

  /**
   * Validate a single block.
   *
   * @param {Object} block Block to validate.
   */
  static validateBlock(block) {
    block.partnerId = null;
    block.partnerIdInvalid = null;
    block.partnerToken = null;
    block.partnerTokenInvalid = null;
    block.violations = null;

    // If no comments are found at all then this can't be a valid block.
    if (block.comments.length === 0) {
      const msg = "Invalid block, no comments found.";
      AllowlistParser.addViolation(block, "error", msg);
      return;
    }

    for (const comment of block.comments) {
      // Check for partner token.
      if (comment.indexOf("!:partner_token=") !== -1) {
        const partnerMatch = comment.match(PARTNER_TOKEN_PATTERN);
        if (partnerMatch) {
          block.partnerToken = partnerMatch[1].trim();
        } else {
          block.partnerTokenInvalid = comment.substr(16);
        }
      } else if (comment.indexOf("!:partner_id=") !== -1) {
        // Check for partner id.
        const partnerIdMatch = comment.match(PARTNER_ID_PATTERN);
        if (partnerIdMatch) {
          block.partnerId = partnerIdMatch[1].trim();
        } else {
          block.partnerIdInvalid = comment.substr(13);
        }
      } else if (comment.indexOf("!:type=") !== -1) {
        // Validate type of the block.
        const typeMatch = comment.match(TYPE_PATTERN);
        if (typeMatch) {
          block.type = typeMatch[1];
        } else {
          const msg = "Invalid type: " + comment.substr(7);
          AllowlistParser.addViolation(block, "error", msg);
        }
      } else if (comment.indexOf("!:forum=") !== -1) {
        // Validate forum link.
        const issueLinkMatch = comment.match(FORUM_LINK_PATTERN);
        if (issueLinkMatch) {
          block.issueLink = issueLinkMatch[1];
        } else if (comment === "!:forum=-none-") {
          block.issueLinkIsEmpty = true;
        } else {
          const msg = "Invalid forum link: " + comment.substr(8);
          AllowlistParser.addViolation(block, "error", msg);
        }
      }
    }

    // Block type need to be set for any kind of block.
    if (!block.type) {
      const msg = "Invalid block, type not set.";
      AllowlistParser.addViolation(block, "error", msg);
      // TODO: Fix early exit.
      return;
    }

    // Check partner specific validations.
    if (block.type === "partner") {
      // Partner ID is required.
      if (!block.partnerId) {
        const msg = block.partnerIdInvalid ? "Invalid partner id: " +
          block.partnerIdInvalid : "Missing partner id.";
        AllowlistParser.addViolation(block, "error", msg);
      }

      // Partner token is required.
      if (!block.partnerToken) {
        const msg = block.partnerTokenInvalid ?
          "Invalid partner token: " + block.partnerTokenInvalid :
          "Missing partner token.";
        AllowlistParser.addViolation(block, "error", msg);
      }

      // Issue link needs to be set via a valid link or an empty tag.
      if (!block.issueLink && !block.issueLinkIsEmpty) {
        AllowlistParser.addViolation(block, "error", "Missing issue link.");
      }
    }

    // Generate a warning as a block should have active rules.
    if (block.rules.length === 0) {
      const msg = "Block without active rules.";
      AllowlistParser.addViolation(block, "warning", msg);
    }
  }

}

module.exports = AllowlistParser;
