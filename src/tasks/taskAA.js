"use strict";

const fs = require("fs");
const path = require("path");

const config = require("config");

const AllowlistParser = require("../lib/allowListParser");

class TaskAA {
  constructor(filterLists) {
    this.filterListRecipes = filterLists;
  }

  #applyRecipe(recipe, block) {
    // Only include non-partner blocks plus partner blocks as specified
    // If no specification is present, include all partners
    // Todo: This will be spec-ed more robustly in recipe files
    const HAS_PARTNER = !!block.partnerId;
    const INCLUDE_PARTNER = !recipe.partnersToInclude
        || recipe.partnersToInclude.has(block.partnerId);
    
    if (!HAS_PARTNER || INCLUDE_PARTNER) {
      block.include = true;
    }
  }

  async generate() {
    const HEADER = await fs.promises.readFile(config.get("output.headerFile"));

    // Create all new filter lists per recipes
    for (const filterListRecipe of this.filterListRecipes) {
      let blocksCombined = [];

      for (const file of filterListRecipe.inputFiles) {
        const content = await fs.promises.readFile(file, "utf-8");
        const lines = content.split("\n");
        const blocks = AllowlistParser.parseBlocks(lines);
  
        // Validate blocks and filter out blocks according to the recipe
        // of the filter list to be created
        for (const block of blocks) {
          AllowlistParser.validateBlock(block);

          const blockHasError = block.violations &&
            block.violations.some(({type}) => type === "error");
          if (blockHasError) {
            console.warn(block.violations);
          }

          this.#applyRecipe(filterListRecipe, block);
        }
 
        blocksCombined.push(...blocks.filter(block => block.include));
      }

      const blockLines = blocksCombined.map(block => block.lines.join("\n") + "\n");
      await fs.promises.writeFile(filterListRecipe.outputFile, HEADER + blockLines.join("\n"));
    }

    return this.filterListRecipes.map(recipe => recipe.outputFile);
  }
}

module.exports = TaskAA;
