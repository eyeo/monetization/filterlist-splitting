

"use strict";

const fs = require("fs");
const path = require("path");

const config = require("config");
const { vol } = require("memfs");

const TaskAA = require("../src/tasks/taskAA");

jest.mock("fs");
const readFile = fs.promises.readFile;

describe("A partner block is correctly filtered", () => {

  const fixtureDir = "./test/__fixtures__/edge_cases";
  const inputDir = "./test/__fixtures__/_input";
  const outputFile = path.join(config.get("output.dir"), "filterlist.created.txt");

  beforeAll(() => {
    fs.use(vol);
  });

  beforeEach(() => {
    vol.reset();
  });

  it("even if empty", async () => {
    const taskUnderTest = new TaskAA([{
      inputFiles: [
        path.join(inputDir, "filterlist-edge_cases.txt")
      ],
      outputFile,
      partnersToInclude: new Set([
        "0123456789abcdeg",
        "0123456789abcdeh"
      ])
    }]);

    await taskUnderTest.generate();

    const expectedFile = (await readFile(path.join(fixtureDir, "empty.expected.txt"))).toString();
    const createdFile = (await readFile(outputFile)).toString();

    expect(createdFile).toEqual(expectedFile);
  });

  it("even if it contains other comments", async () => {
    const taskUnderTest = new TaskAA([{
      inputFiles: [
        path.join(inputDir, "filterlist-edge_cases.txt")
      ],
      outputFile,
      partnersToInclude: new Set([
        "0123456789abcdef",
        "0123456789abcdeh"
      ])
    }]);

    await taskUnderTest.generate();

    const expectedFile = (await readFile(path.join(fixtureDir, "extra_comments.expected.txt"))).toString();
    const createdFile = (await readFile(outputFile)).toString();

    expect(createdFile).toEqual(expectedFile);
  });

  it("even if it only contains comments", async () => {
    const taskUnderTest = new TaskAA([{
      inputFiles: [
        path.join(inputDir, "filterlist-edge_cases.txt"),
      ],
      outputFile,
      partnersToInclude: new Set([
        "0123456789abcdef",
        "0123456789abcdeg"
      ])
    }]);

    await taskUnderTest.generate();

    const expectedFile = (await readFile(path.join(fixtureDir, "comments.expected.txt"))).toString();
    const createdFile = (await readFile(outputFile)).toString();

    expect(createdFile).toEqual(expectedFile);
  });
});
