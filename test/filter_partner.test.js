"use strict";

const fs = require("fs");
const path = require("path");

const config = require("config");
const { vol } = require("memfs");

const TaskAA = require("../src/tasks/taskAA");

jest.mock("fs");
const readFile = fs.promises.readFile;

describe("A partner block is correctly filtered", () => {

  const fixtureDir = "./test/__fixtures__/filter_partner";
  const inputDir = "./test/__fixtures__/_input";
  const outputFile = path.join(config.get("output.dir"), "filterlist.created.txt");

  beforeAll(() => {
    fs.use(vol);
  });

  beforeEach(() => {
    vol.reset();
  });

  it("from the start of an input file", async () => {
    const taskUnderTest = new TaskAA([{
      inputFiles: [
        path.join(inputDir, "filterlist1.txt")
      ],
      outputFile,
      partnersToInclude: new Set([
        "0123456789abcdeg",
        "0123456789abcdeh"
      ])
    }]);

    await taskUnderTest.generate();

    const expectedFile = (await readFile(path.join(fixtureDir, "start.expected.txt"))).toString();
    const createdFile = (await readFile(outputFile)).toString();

    expect(createdFile).toEqual(expectedFile);
  });

  it("from the middle of an input file", async () => {
    const taskUnderTest = new TaskAA([{
      inputFiles: [
        path.join(inputDir, "filterlist1.txt")
      ],
      outputFile,
      partnersToInclude: new Set([
        "0123456789abcdef",
        "0123456789abcdeh"
      ])
    }]);

    await taskUnderTest.generate();

    const expectedFile = (await readFile(path.join(fixtureDir, "middle.expected.txt"))).toString();
    const createdFile = (await readFile(outputFile)).toString();

    expect(createdFile).toEqual(expectedFile);
  });

  it("from the end of an input file", async () => {
    const taskUnderTest = new TaskAA([{
      inputFiles: [
        path.join(inputDir, "filterlist1.txt")
      ],
      outputFile,
      partnersToInclude: new Set([
        "0123456789abcdef",
        "0123456789abcdeg"
      ])
    }]);

    await taskUnderTest.generate();

    const expectedFile = (await readFile(path.join(fixtureDir, "end.expected.txt"))).toString();
    const createdFile = (await readFile(outputFile)).toString();

    expect(createdFile).toEqual(expectedFile);
  });

  it("across multiple input files", async () => {
    const taskUnderTest = new TaskAA([{
      inputFiles: [
        path.join(inputDir, "filterlist1.txt"),
        path.join(inputDir, "filterlist2.txt")
      ],
      outputFile,
      partnersToInclude: new Set([
        "0123456789abcdef",
        "0123456789abcdeh"
      ])
    }]);

    await taskUnderTest.generate();

    const expectedFile = (await readFile(path.join(fixtureDir, "multiple.expected.txt"))).toString();
    const createdFile = (await readFile(outputFile)).toString();

    expect(createdFile).toEqual(expectedFile);
  });
});
