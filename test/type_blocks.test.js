"use strict";

const fs = require("fs");
const path = require("path");

const config = require("config");
const { vol } = require("memfs");

const TaskAA = require("../src/tasks/taskAA");

jest.mock("fs");
const readFile = fs.promises.readFile;

describe("Qualifying `type` blocks are preserved", () => {

  const fixtureDir = "./test/__fixtures__/type_blocks";
  const inputDir = "./test/__fixtures__/_input";
  const outputFile = path.join(config.get("output.dir"), "filterlist.created.txt");

  beforeAll(() => {
    fs.use(vol);
  });

  beforeEach(() => {
    vol.reset();
  });

  it("if a partner block before them is filtered out", async () => {
    const taskUnderTest = new TaskAA([{
      inputFiles: [
        path.join(inputDir, "filterlist-type_blocks.txt")
      ],
      outputFile,
      partnersToInclude: new Set([
        "0123456789abcdeg"
      ])
    }]);

    await taskUnderTest.generate();

    const expectedFile = (await readFile(path.join(fixtureDir, "before.expected.txt"))).toString();
    const createdFile = (await readFile(outputFile)).toString();

    expect(createdFile).toEqual(expectedFile);
  });

  it("if a partner block behind them is filtered out", async () => {
    const taskUnderTest = new TaskAA([{
      inputFiles: [
        path.join(inputDir, "filterlist-type_blocks.txt")
      ],
      outputFile,
      partnersToInclude: new Set([
        "0123456789abcdef"
      ])
    }]);

    await taskUnderTest.generate();

    const expectedFile = (await readFile(path.join(fixtureDir, "behind.expected.txt"))).toString();
    const createdFile = (await readFile(outputFile)).toString();

    expect(createdFile).toEqual(expectedFile);
  });
});
