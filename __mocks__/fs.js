const fs = jest.requireActual("fs");
const unionfs = require("unionfs").default;

// Mock the filesystem with UnionFS that combines the actual filesystem
// with additional virtual volumes on top
module.exports = unionfs.use(fs);
